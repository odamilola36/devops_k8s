FROM adoptopenjdk/openjdk8:alpine

RUN adduser -D lomari

USER lomari

WORKDIR /app

ENV PROFILE=${PROFILE}

COPY target/*.jar /app/

# COPY target/*.jar /app/

EXPOSE 8090

ENTRYPOINT java -jar -Dspring.profiles.active=${PROFILE} /app/*.jar
