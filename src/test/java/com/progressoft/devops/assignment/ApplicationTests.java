package com.progressoft.devops.assignment;


import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;

@ActiveProfiles("h2")
@SpringBootTest
public class ApplicationTests {

    @Test
    void contextLoads() {
    }
}